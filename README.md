# JavaChess
This is a project made by murinmat studying at CTU in Prague.
It is meant to be a very simple chess GUI game for 1 or 2 players.
The implemented AI uses MiniMax algorithm with Alpha-Beta pruning.

## Compilation and runnning the game
Ideally, you would open this project inside your intellij and it will take care of it, if you want to compile it yourself, to so as follows:
  - To compile, running ``javac main/Main.java`` from the src folder should be enough.
  - To run the game, be inside the src folder and run it via ``java main.Main``. The game needs to be compiled first.

## Notes
Every time you want to start a game, you have to create a new game by either 'CTRL-N' or using GUI and then start the game either via 'CTRL-S' or using the GUI.

## Current to-do's:
  - Add the option to remove save files
  - Possibly change the way user chooses a save file to be loaded

## Known bugs:
  - :)

