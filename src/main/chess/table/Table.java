package main.chess.table;

import main.chess.utils.Move;
import main.chess.pieces.King;
import main.chess.pieces.Piece;
import main.chess.utils.ChessColor;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Implementation of a chess main.chess.table.
 */
public class Table implements Serializable {
    /**
     * Constructs the chess table from scratch, placing all the initial units on the board.
     */
    public Table(){
        m_Board = new Tile[8][8];
        for(int i = 0; i < 8; ++i)
            for(int j = 0; j < 8; ++j)
                m_Board[i][j] = new Tile(j, i, null);
    }

    /**
     * Construct the table from another table, making a deep copy
     * @param other is the table that is being copied
     */
    public Table(Table other) {
        m_Board = new Tile[8][8];
        for(int i = 0; i < 8; ++i) {
            for(int j = 0; j < 8; ++j) {
                m_Board[i][j] = new Tile(j, i, null);
                if (other.getTile(j, i).isTaken()) {
                    m_Board[i][j].placePiece(other.getTile(j, i).piece().get().clone(m_Board[i][j]));
                    m_Board[i][j].piece().get().setTile(m_Board[i][j]);
                }
            }
        }
    }

    /**
     * Get the tile of a coordinate given by params.
     * @param column is the column of the tile
     * @param row is the row of the tile
     * @return the tile of given coordinates
     */
    public Tile getTile(int column, int row) {
        if (column < 0 || column > 7 || row < 0 || row > 7)
            throw new IllegalArgumentException();
        return m_Board[row][column];
    }


    /**
     * Make a move on the current game
     * @param m is the move to be done
     */
    public void makeMove(Move m, ChessColor currentPlayer) {
        Tile fromTile = getTile(m.fromX, m.fromY);
        Tile toTile = getTile(m.toX, m.toY);
        Optional<Piece> pieceToMove = fromTile.piece();
        if (!(pieceToMove.isPresent()))
            throw new IllegalStateException("Tile to make a move from is empty!");
        if (!pieceToMove.get().canMoveTo(toTile, this))
            throw new IllegalArgumentException("Illegal Move!");
        if (pieceToMove.get().color() != currentPlayer)
            throw new IllegalArgumentException("Not this players turn!");

        Optional<Piece> toTake = pieceToMove.get().takeAfterMove(toTile, this);

        // control king castling
        if (pieceToMove.get() instanceof King) {
            Piece king = pieceToMove.get();
            if (king.movesTaken() == 0) {
                if (m.toX == 1) {
                    Piece rookToMove = getTile(0, m.toY).piece().get();
                    getTile(0, m.toY).removePiece();
                    getTile(2, m.toY).placePiece(rookToMove);
                    rookToMove.moveTo(getTile(2, m.toY));
                }
                else if (m.toX == 5) {
                    Piece rookToMove = getTile(7, m.toY).piece().get();
                    getTile(7, m.toY).removePiece();
                    getTile(4, m.toY).placePiece(rookToMove);
                    rookToMove.moveTo(getTile(4, m.toY));
                }
            }
        }

        // remove piece to where we are moving if there is any
        toTake.ifPresent(piece -> piece.getTile().removePiece());
        // place the new piece and empty it's old position
        toTile.placePiece(fromTile.piece().get());
        fromTile.piece().get().moveTo(toTile);
        fromTile.removePiece();
    }

    /**
     * Get list of active pieces
     * @return list of active pieces
     */
    public List<Piece> getPieces() {
        List<Piece> tmp = new ArrayList<>();
        for(int i = 0; i < 8; ++i) {
            for(int j = 0; j < 8; ++j) {
                if (m_Board[i][j].piece().isPresent())
                    tmp.add(m_Board[i][j].piece().get());
            }
        }

        return tmp;
    }

    /**
     * Get all the valid moves for given player on the table
     * @param playerTurn is defining which player's turn it is
     * @return array list of valid moves
     */
    public List<Move> getLegalMoves(ChessColor playerTurn) {
        // get all the valid moves
        ArrayList<Move> moves = new ArrayList<>();
        for(Piece p : getPieces()) {
            // skip pieces of different color
            if (!p.color().toString().equals(playerTurn.toString()))
                continue;
            for(int i = 0; i < 8; ++i) {
                for(int j = 0; j < 8; ++j) {
                    if (!p.canMoveTo(getTile(j, i), this))
                        continue;

                    Table tmp = new Table(this);
                    tmp.makeMove(new Move(p.getTile().column(), p.getTile().row(), j, i), playerTurn);
                    if (tmp.isKingSafe(playerTurn))
                        moves.add(new Move(p.getTile().column(), p.getTile().row(), j, i));
                }
            }
        }

        return moves;
    }


    /**
     * Check if King of the opposite color of opponent is safe or not on a given table
     * @param kingColor is the color of the king we are checking
     * @return boolean if king is safe after such a move
     */
    public boolean isKingSafe(ChessColor kingColor) {
        Tile kingTile = getKingTile(kingColor);
        for(Piece p : getPieces()) {
            // skip pieces of different color
            if (p.color().equals(kingColor))
                continue;
            for(int i = 0; i < 8; ++i)
                for(int j = 0; j < 8; ++j)
                    if(p.canMoveTo(kingTile, this))
                        return false;
        }

        return true;
    }

    /**
     * Find king tile of this board with given color
     * @param c is the color
     * @return the tile the king is standing on
     */
    private Tile getKingTile(ChessColor c)  {
        for(int i = 0; i < 8; ++i) {
            for(int j = 0; j < 8; ++j) {
                Tile t = getTile(j, i);
                if (t.piece().isPresent() && t.piece().get().color() == c && t.piece().get() instanceof King)
                    return t;
            }
        }

        System.out.println("WTF KING NOT FOUND!!!");
        return null;
    }

    private Tile[][] m_Board;
}
