package main.chess.table;

import main.chess.pieces.Piece;

import java.io.Serializable;
import java.util.Optional;

/**
 * Class defining a chess tile.
 */
public class Tile implements Serializable {
    /**
     * Constructor for a chess tile.
     * @param x defines column of the tile
     * @param y defines row of the tile
     * @param piece is the piece to be initially placed on the tile
     */
    public Tile(int x, int y, Piece piece){
        m_Column = x;
        m_Row = y;
        m_Piece = piece;
    }

    /**
     * Binary value if the tile is occupied by a piece.
     * @return true if tile is occupied, else false
     */
    public boolean isTaken(){
        return piece().isPresent();
    }

    /**
     * Get the piece that is currently standing on the tile, if any
     * @return the current piece on tile
     */
    public Optional<Piece> piece(){
        if (m_Piece == null)
            return Optional.empty();
        return Optional.of(m_Piece);
    }

    /**
     * Remove a piece from this file, if any is here.
     * @return the removed piece
     */
    public Piece removePiece(){
        if (m_Piece == null)
            throw new IllegalStateException("Can't remove a piece from an empty tile!");
        Piece p = m_Piece;
        m_Piece = null;
        return p;
    }

    /**
     * Places a piece on the tile.
     * @param p is the piece to be placed
     */
    public void placePiece(Piece p){
        m_Piece = p;
    }

    /**
     * Getter for x coordinate of the tile.
     * @return x value of the tile.
     */
    public int column(){
        return m_Column;
    }

    /**
     * Getter for y coordinate of the tile.
     * @return y value of the tile.
     */
    public int row(){
        return m_Row;
    }

    /**
     * Compare two tiles if they are the same.
     * @param obj is the other tile
     * @return true if the coordinates match
     */
    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Tile))
            return false;
        Tile tmp = (Tile)obj;
        return m_Column == tmp.column() && m_Row == tmp.row();
    }

    private final int               m_Column;
    private final int               m_Row;
    private Piece                   m_Piece;
}
