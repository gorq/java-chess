package main.chess.pieces;

import main.chess.table.Table;
import main.chess.table.Tile;
import main.chess.utils.ChessColor;
import java.util.Optional;

/**
 * Definition of a chess pawn.
 */
public class Pawn extends Piece{
    /**
     * {@inheritDoc}
     */
    public Pawn(Tile tile, ChessColor pieceColor, int movesTaken) {
        super(tile, pieceColor, movesTaken);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Piece clone(Tile t) {
        return new Pawn(t, m_PieceColor, m_MovesTaken);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean canMoveTo(Tile to, Table t) {
        if(m_Tile.equals(to))
            return false;
        int direction = m_PieceColor == ChessColor.WHITE ? -1 : 1;
        // if moving only forward
        if(to.column() == m_Tile.column()) {
            boolean validBy2 = m_Tile.row()+(2*direction) == to.row()
                        && !to.isTaken()
                        && m_MovesTaken == 0
                        && !t.getTile(m_Tile.column(), m_Tile.row()+direction).isTaken();
            boolean validBy1 = m_Tile.row()+direction == to.row() && !to.isTaken();
            return validBy1 || validBy2;
        }
        // else we are moving to the side, which means we are taking a unit
        if(to.isTaken()) {
            return to.piece().isPresent()
                    && to.piece().get().color() != m_PieceColor
                    && m_Tile.row() - to.row() == -direction
                    && Math.abs(m_Tile.column() - to.column()) == 1;
        }
        // else we can only take en passant
        else {
            Tile toTake = t.getTile(to.column(), m_Tile.row());
            return m_Tile.row() == toTake.row()
                    && m_Tile.row() - to.row() == -direction
                    && Math.abs(m_Tile.column() - toTake.column()) == 1
                    && toTake.isTaken()
                    && toTake.piece().get().color() != m_PieceColor
                    && toTake.piece().get().movesTaken() == 1
                    && toTake.piece().get() instanceof Pawn;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Optional<Piece> takeAfterMove(Tile to, Table t) {
        if (!canMoveTo(to, t))
            throw new IllegalArgumentException();

        if(to.isTaken())
            return Optional.of(to.piece().get());

        // check en passant
        Tile toTake = t.getTile(to.column(), m_Tile.row());
        if (m_Tile.row() == toTake.row()
                && toTake.isTaken()
                && toTake.piece().get().color() != m_PieceColor
                && toTake.piece().get().movesTaken() == 1
                && toTake.piece().get() instanceof Pawn)
            return Optional.of(toTake.piece().get());
        return Optional.empty();
    }

    /**
     * {@inheritDoc}
     * @return value of pawn (1)
     */
    @Override
    public int value() {
        return 1;
    }
}
