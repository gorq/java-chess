package main.chess.pieces;

import main.chess.table.Table;
import main.chess.table.Tile;
import main.chess.utils.ChessColor;

import java.util.Optional;

/**
 * Definition of chess queen.
 */
public class Queen extends Piece {
    /**
     * {@inheritDoc}
     */
    public Queen(Tile tile, ChessColor pieceColor, int movesTaken) {
        super(tile, pieceColor, movesTaken);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Piece clone(Tile t) {
        return new Queen(t, m_PieceColor, m_MovesTaken);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean canMoveTo(Tile to, Table t) {
        if (m_Tile.equals(to))
            return false;
        // check move like rook
        if (m_Tile.column() == to.column()) {
            int current = m_Tile.row() + (to.row() < m_Tile.row() ? -1 : 1);
            while (current != to.row()) {
                if (t.getTile(m_Tile.column(), current).isTaken())
                    return false;
                current += to.row() < m_Tile.row() ? -1 : 1;
            }
            if (t.getTile(m_Tile.column(), current).isTaken()) {
                return t.getTile(m_Tile.column(), current).piece().get().color() != m_PieceColor;
            }
            return true;
        } else if (m_Tile.row() == to.row()) {
            int current = m_Tile.column() + (to.column() < m_Tile.column() ? -1 : 1);
            while (current != to.column()) {
                if (t.getTile(current, m_Tile.row()).isTaken())
                    return false;
                current += to.column() < m_Tile.column() ? -1 : 1;
            }
            if (t.getTile(current, m_Tile.row()).isTaken())
                return t.getTile(current, m_Tile.row()).piece().get().color() != m_PieceColor;
            return true;
        }
        // else check move like bishop
        if ((Math.abs(m_Tile.row() - to.row()) - Math.abs(m_Tile.column() - to.column())) != 0)
            return false;
        int xDirection = m_Tile.column() < to.column() ? 1 : -1;
        int yDirection = m_Tile.row() < to.row() ? 1 : -1;
        int currentX = m_Tile.column() + xDirection;
        int currentY = m_Tile.row() + yDirection;
        while (currentX != to.column()) {
            if (t.getTile(currentX, currentY).isTaken())
                return false;
            currentX += xDirection;
            currentY += yDirection;
        }
        // get piece
        Optional<Piece> toTake = t.getTile(currentX, currentY).piece();
        return !(toTake.isPresent()) || toTake.get().color() != m_PieceColor;
    }

    /**
     * {@inheritDoc}
     * @return the value of queen (9)
     */
    @Override
    public int value() {
        return 9;
    }
}
