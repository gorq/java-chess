package main.chess.pieces;

import main.chess.table.Table;
import main.chess.table.Tile;
import main.chess.utils.ChessColor;

import java.util.Optional;

/**
 * Definition of a chess king.
 */
public class King extends Piece{
    /**
     * {@inheritDoc}
     */
    public King(Tile tile, ChessColor pieceColor, int movesTaken) {
        super(tile, pieceColor, movesTaken);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Piece clone(Tile t) {
        return new King(t, m_PieceColor, m_MovesTaken);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean canMoveTo(Tile to, Table t) {
        if (m_Tile.equals(to))
            return false;
        if(to.piece().isPresent() && to.piece().get().color() == m_PieceColor)
            return false;
        // check it we are castling
        if (
                m_MovesTaken == 0
                && m_Tile.row() == to.row()
                && (to.column() == 1 || to.column() == 5)
        ) {
            // get the rook that would move
            Optional<Piece> movingRook = t.getTile(to.column() == 1 ? 0 : 7, to.row()).piece();
            if (to.column() == 5 ) {
                if (t.getTile(4, m_Tile.row()).isTaken() || t.getTile(6, m_Tile.row()).isTaken())
                    return false;
            }
            else if(t.getTile(2, m_Tile.row()).isTaken())
                return false;
            return (
                    !to.isTaken()
                    && movingRook.isPresent()
                    && movingRook.get() instanceof Rook
                    && movingRook.get().color() == m_PieceColor
                    && movingRook.get().movesTaken() == 0
            );
        }
        // else check regular moving king
        return Math.abs(m_Tile.column()-to.column()) <= 1
                && Math.abs(m_Tile.row()-to.row()) <= 1;
    }

    /**
     * {@inheritDoc}
     * @return the value of king (1000)
     */
    @Override
    public int value() {
        return 1000;
    }
}
