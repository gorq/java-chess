package main.chess.pieces;

import main.chess.table.Table;
import main.chess.table.Tile;
import main.chess.utils.ChessColor;

import java.util.Optional;

/**
 * Definition of chess Bishop
 */
public class Bishop extends Piece{
    /**
     * {@inheritDoc}
     */
    public Bishop(Tile tile, ChessColor pieceColor, int movesTaken) {
        super(tile, pieceColor, movesTaken);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Piece clone(Tile t) {
        return new Bishop(t, m_PieceColor, m_MovesTaken);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean canMoveTo(Tile to, Table t) {
        if (m_Tile.equals(to))
            return false;
        // check if it's a diagonal move
        if ((Math.abs(m_Tile.row() - to.row()) - Math.abs(m_Tile.column() - to.column())) != 0)
            return false;
        int xDirection = m_Tile.column() < to.column() ? 1 : -1;
        int yDirection = m_Tile.row() < to.row() ? 1 : -1;
        int currentX = m_Tile.column() + xDirection;
        int currentY = m_Tile.row() + yDirection;

        while (currentX != to.column()) {
            if (t.getTile(currentX, currentY).isTaken())
                return false;
            currentX += xDirection;
            currentY += yDirection;
        }
        // get piece
        Optional<Piece> toTake = t.getTile(currentX, currentY).piece();
        return !(toTake.isPresent()) || toTake.get().color() != m_PieceColor;
    }

    /**
     * {@inheritDoc}
     * @return the value of bishop (3)
     */
    @Override
    public int value() {
        return 3;
    }
}
