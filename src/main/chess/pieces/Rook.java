package main.chess.pieces;

import main.chess.table.Table;
import main.chess.table.Tile;
import main.chess.utils.ChessColor;

/**
 * Definition of a chess rook.
 */
public class Rook extends Piece{
    /**
     * {@inheritDoc}
     */
    public Rook(Tile tile, ChessColor pieceColor, int movesTaken) {
        super(tile, pieceColor, movesTaken);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Piece clone(Tile t) {
        return new Rook(t, m_PieceColor, m_MovesTaken);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean canMoveTo(Tile to, Table t) {
        if (m_Tile == to)
            return false;
        if(m_Tile.column() == to.column()) {
            int current = m_Tile.row()+(to.row() < m_Tile.row() ? -1 : 1);
            while (current != to.row()) {
                if(t.getTile(m_Tile.column(), current).isTaken())
                    return false;
                current += to.row() < m_Tile.row() ? -1 : 1;
            }
            if (t.getTile(m_Tile.column(), current).isTaken()) {
                return t.getTile(m_Tile.column(), current).piece().get().color() != m_PieceColor;
            }
            return true;
        }
        else if (m_Tile.row() == to.row()) {
            int current = m_Tile.column()+(to.column() < m_Tile.column() ? -1 : 1);
            while (current != to.column()) {
                if(t.getTile(current, m_Tile.row()).isTaken())
                    return false;
                current += to.column() < m_Tile.column() ? -1 : 1;
            }
            if (t.getTile(current, m_Tile.row()).isTaken())
                return t.getTile(current, m_Tile.row()).piece().get().color() != m_PieceColor;
            return true;
        }
        return false;
    }

    /**
     * {@inheritDoc}
     * @return rook value (5)
     */
    @Override
    public int value() {
        return 5;
    }
}
