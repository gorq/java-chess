package main.chess.pieces;

import main.chess.table.Table;
import main.chess.utils.ChessColor;
import main.chess.table.Tile;

import java.io.Serializable;
import java.util.Optional;

/**
 * An abstract class for a chess piece defining all it's mandatory methods.
 */
public abstract class Piece implements Serializable {
    /**
     * Constructor for a chess piece.
     * @param tile is the tile on which the piece is created
     * @param pieceColor is the color of the piece being made
     */
    public Piece(Tile tile, ChessColor pieceColor, int movesTaken){
        m_Tile = tile;
        m_PieceColor = pieceColor;
        m_MovesTaken = movesTaken;
    }

    /**
     * Make a deep copy of this piece
     * @param t is the new tile to be placed on
     * @return deep copy of this piece placed on new tile
     */
    public abstract Piece clone(Tile t);

    /**
     * Getter for tile on which the piece is standing.
     * @return the current tile on which the piece is standing
     */
    public final Tile getTile() {
        return m_Tile;
    }

    /**
     * Getter for a piece color.
     * @return the color if the piece
     */
    public final ChessColor color(){
        return m_PieceColor;
    }

    /**
     * Getter for moves taken by given piece.
     * @return number of moves taken
     */
    public final int movesTaken(){
        return m_MovesTaken;
    }

    /**
     * Logic of the piece, if it can move from a tile
     * @param to is the target position of piece
     * @param t is the main.chess.table upon which the piece is to move
     * @return true if such a move is possible
     */
    public abstract boolean canMoveTo(Tile to, Table t);

    /**
     * Move a piece to a given tile
     * @param t is the tile to which to move the piece to
     */
    public final void moveTo(Tile t) {
        m_Tile = t;
        m_MovesTaken += 1;
    }

    /**
     * Set the tile of this piece to a new tile, without increasing move count
     * @param t is the new tile
     */
    public final void setTile(Tile t) {
        m_Tile = t;
    }

    /**
     * Get a piece to take if a given move is made.
     * @param to is the target position of piece
     * @param t is the main.chess.table upon which the piece is to move
     * @return piece to be taken, if any
     */
    public Optional<Piece> takeAfterMove(Tile to, Table t) {
        if(!canMoveTo(to, t))
            throw new IllegalArgumentException();
        return to.piece();
    }

    /**
     * Value of the piece defined by standard valuation.
     * @return it's value
     */
    public abstract int value();

    /**
     * Get the name of this class.
     * @return class name as string
     */
    @Override
    public String toString() {
        return this.getClass().getSimpleName();
    }

    protected ChessColor m_PieceColor;
    protected Tile                              m_Tile;
    protected int                               m_MovesTaken;
}
