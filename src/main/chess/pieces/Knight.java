package main.chess.pieces;

import main.chess.table.Table;
import main.chess.table.Tile;
import main.chess.utils.ChessColor;

/**
 * Definition of a chess knight.
 */
public class Knight extends Piece{
    /**
     * {@inheritDoc}
     */
    public Knight(Tile tile, ChessColor pieceColor, int movesTaken){
        super(tile, pieceColor, movesTaken);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Piece clone(Tile t) {
        return new Knight(t, m_PieceColor, m_MovesTaken);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean canMoveTo(Tile to, Table t) {
        return
                (!to.isTaken() || to.piece().get().color() != m_PieceColor)
                && (
                    (Math.abs(m_Tile.column() - to.column()) == 2 && Math.abs(m_Tile.row() - to.row()) == 1)
                    ||
                    (Math.abs(m_Tile.column() - to.column()) == 1 && Math.abs(m_Tile.row() - to.row()) == 2)
                );

    }

    /**
     * {@inheritDoc}
     * @return the value of knight (3)
     */
    @Override
    public int value() {
        return 3;
    }
}
