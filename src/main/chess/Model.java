package main.chess;

import main.chess.engine.Chess;
import main.chess.engine.GameState;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Model {
    public static List<String> EXISTING_SAVES_NAMES;
    public static Chess CHESS;

    static {
        reloadSaves();
    }

    public static void reloadSaves() {
        EXISTING_SAVES_NAMES = getExistingSavesNames();
    }

    /**
    * Get all the save file names
    * @return list of save file names
    */
    private static List<String> getExistingSavesNames() {
        List<String> result = new ArrayList<>();
        File currentFolder = new File(".");
        for( File f : Objects.requireNonNull(currentFolder.listFiles())) {
            if (f.getName().equals("saves")) {
                for (File save : Objects.requireNonNull(new File(f.getPath()).listFiles()))
                    result.add(save.getName());
                return result;
            }
        }
        if (!new File("./saves").mkdirs())
            throw new IllegalStateException("Could not find or create save directory!");

        return result;
    }

    public static void deleteSave(String saveName) {
        new File("saves/"+saveName).delete();
        reloadSaves();
    }
}
