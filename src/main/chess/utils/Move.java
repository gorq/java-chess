package main.chess.utils;

/**
 * Class defining a chess move.
 */
public class Move {
    public int fromX, fromY, toX, toY;

    /**
     * A simple comstructor
     * @param fx is the from-X coordinate
     * @param fy is the from-Y coordinate
     * @param tx is the to-X coordinate
     * @param ty is the to-Y coordinate
     */
    public Move(int fx, int fy, int tx, int ty) {
        fromX = fx;
        fromY = fy;
        toX = tx;
        toY = ty;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Move))
                return false;
        Move m = (Move)obj;
        return fromX == m.fromX && fromY == m.fromY && toX == m.toX && toY == m.toY;
    }

    @Override
    public String toString() {
        return "From x: " + fromX + ", from y: " + fromY + ", to x: " + toX + ", to y: " + toY;
    }
}
