package main.chess.utils;

/**
 * Opponent type enumeration.
 */
public enum PlayerType {
    PERSON, AI;
}
