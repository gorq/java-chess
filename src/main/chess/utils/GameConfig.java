package main.chess.utils;

public final class GameConfig {
    public GameConfig(int difficulty, PlayerType white, PlayerType black) {
        this.difficulty = difficulty;
        this.white = white;
        this.black = black;
    }

    public int difficulty(){ return difficulty; }
    public PlayerType whiteType(){ return white; }
    public PlayerType blackType(){ return black; }

    private final int difficulty;
    private final PlayerType white;
    private final PlayerType black;
}
