package main.chess.utils;

/**
 * Enumerator for a chess piece color.
 */
public enum ChessColor {
    BLACK, WHITE;
    public static ChessColor swapColor(ChessColor c){
        return c == BLACK ? WHITE : BLACK;
    }
}
