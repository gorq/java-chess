package main.chess.engine;

import javafx.scene.control.Button;
import main.chess.pieces.*;
import main.chess.utils.Move;
import main.chess.utils.PlayerType;
import main.gui.ChessGUI;
import main.chess.table.Table;
import main.chess.table.Tile;
import main.chess.utils.ChessColor;
import main.chess.utils.GameConfig;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;

/**
 * Chess engine controller.
 */
public final class Chess {
    /**
     * Simple constructor
     * @param gc is the gui controller
     * @param buttons is the buttons that are to be clicked on board
     * @param boardChangeLock is the board change lock for concurrency
     */
    public Chess(ChessGUI gc, Button[][] buttons, Lock boardChangeLock, Condition nextMoveCond) {
        this.guiController = gc;
        this.boardChangeLock = boardChangeLock;
        this.awaitNextAIMoveCondition = nextMoveCond;
        Boolean isPlaying = false;
    }

    /**
     * Method to start a new game.
     * @param cfg is the config to start with.
     */
    public void startNewGame(GameConfig cfg) {
        gameState = new GameState();
        gameState.setAISearchDepth(cfg.difficulty());
        if (cfg.whiteType() == PlayerType.PERSON)
            gameState.setWhiteAsHuman();
        else
            gameState.setWhiteAsAI();
        if (cfg.blackType() == PlayerType.PERSON)
            gameState.setBlackAsHuman();
        else
            gameState.setBlackAsAI();
        initPieces();
        guiController.updateBoard();
        gameState.signalGameStart();
        startMainLoop();
    }

    /**
     * Start a game from a game state.
     * @param gs is the game state to start from.
     */
    public void loadGame(GameState gs) {
        this.gameState = gs;
        startMainLoop();
    }

    /**
     * Start main loop.
     */
    private void startMainLoop() {
        this.moveController = new MoveController(gameState, guiController, boardChangeLock, awaitNextAIMoveCondition);
        moveController.start();
    }

    /**
     * Interrupt a move controller.
     * This is to stop calculation when asked for by GUI controller.
     */
    public void interruptMoveController(){
        if (moveController != null)
            moveController.interrupt();
    }

    /**
     * Make a move on the current game
     * @param m is the move to be done
     */
    public void makeMove(Move m) {
        gameState.update(m);
        guiController.updateBoard();
    }

    /**
     * Getter for game state.
     * @return the game state of this class
     */
    public GameState gameState() { return gameState; }

    /**
     * Initialize all pieces.
     */
    public void initPieces() {
        Tile t;
        Table table = gameState.table();
        // pawns
        for(int i = 0; i < 8; ++i) {
            t = table.getTile(i, 1);
            t.placePiece(new Pawn(t, ChessColor.BLACK, 0));
            t = table.getTile(i, 6);
            t.placePiece(new Pawn(t, ChessColor.WHITE, 0));
        }


        // knights
        table.getTile(1, 0).placePiece(new Knight(table.getTile(1, 0), ChessColor.BLACK,0));
        table.getTile(6, 0).placePiece(new Knight(table.getTile(6, 0), ChessColor.BLACK,0));
        table.getTile(1, 7).placePiece(new Knight(table.getTile(1, 7), ChessColor.WHITE,0));
        table.getTile(6, 7).placePiece(new Knight(table.getTile(6, 7), ChessColor.WHITE,0));

        // bishops
        table.getTile(2, 0).placePiece(new Bishop(table.getTile(2, 0), ChessColor.BLACK,0));
        table.getTile(5, 0).placePiece(new Bishop(table.getTile(5, 0), ChessColor.BLACK,0));
        table.getTile(2, 7).placePiece(new Bishop(table.getTile(2, 7), ChessColor.WHITE,0));
        table.getTile(5, 7).placePiece(new Bishop(table.getTile(5, 7), ChessColor.WHITE,0));

        // rooks
        table.getTile(0, 0).placePiece(new Rook(table.getTile(0, 0), ChessColor.BLACK,0));
        table.getTile(7, 0).placePiece(new Rook(table.getTile(7, 0), ChessColor.BLACK,0));
        table.getTile(0, 7).placePiece(new Rook(table.getTile(0, 7), ChessColor.WHITE,0));
        table.getTile(7, 7).placePiece(new Rook(table.getTile(7, 7), ChessColor.WHITE,0));

        // queens
        table.getTile(4, 0).placePiece(new Queen(table.getTile(4, 0), ChessColor.BLACK,0));
        table.getTile(4, 7).placePiece(new Queen(table.getTile(4, 7), ChessColor.WHITE,0));

        // kings
        table.getTile(3, 0).placePiece(new King(table.getTile(3, 0), ChessColor.BLACK,0));
        table.getTile(3, 7).placePiece(new King(table.getTile(3, 7), ChessColor.WHITE,0));
    }

    /**
     * Check king safety after such a move
     * @param m is the move to be made
     * @return true if king will be safe
     */
    public boolean willKingBeSafe(Move m) {
        Table t = new Table(gameState.table());
        t.makeMove(m, gameState.getCurrentPlayerColor());
        return t.isKingSafe(gameState.getCurrentPlayerColor());
    }


    private final ChessGUI guiController;
    private final Lock boardChangeLock;
    private final Condition awaitNextAIMoveCondition;
    private GameState gameState;
    private MoveController moveController;
}
