package main.chess.engine;

import main.chess.engine.AI.ChessAI;
import main.chess.engine.AI.MiniMax;
import main.chess.table.Table;
import main.chess.utils.ChessColor;
import main.chess.utils.Move;

import java.io.Serializable;
import java.util.Optional;

/**
 * A class that represents a game state that is able to be saved into a file.
 */
public class GameState implements Serializable {
    /**
     * Default constructor for GameState, that's prepares the game to a default state
     */
    public GameState() {
        resetToDefault();
        AISearchDepth = 1;
    }

    /**
     * Reset the game state to default values, without changing the AI search depth
     */
    public void resetToDefault() {
        playerTurn = ChessColor.WHITE;
        whitePlayer = null;
        blackPlayer = new MiniMax();
        table = new Table();
        gameStarted = false;
    }

    /**
     * Update the game state by a given move
     * @param m is the move that is being made
     */
    public void update(Move m) {
        table.makeMove(m, playerTurn);
        playerTurn = ChessColor.swapColor(playerTurn);
        currentPlayer = currentPlayer == blackPlayer ? whitePlayer : blackPlayer;
        gameStarted = true;
    }

    /**
     * Set white as MiniMax algorithm
     */
    public void setWhiteAsAI() {
        whitePlayer = new MiniMax();
    }

    /**
     * Set black as MiniMax algorithm
     */
    public void setBlackAsAI() {
        blackPlayer = new MiniMax();
    }

    /**
     * Set white as human (null)
     */
    public void setWhiteAsHuman() {
        whitePlayer = null;
    }

    /**
     * Set black as human (null)
     */
    public void setBlackAsHuman() {
        blackPlayer = null;
    }

    /**
     * Getter for whether the game has started or not
     * @return boolean if game has started or not
     */
    public boolean hasStarted() {
        return gameStarted;
    }

    /**
     * Flag for whether the game as ended or not
     * @return true if game has ended
     */
    public boolean hasEnded() {
        return isCheckMate() || isStaleMate();
    }

    /**
     * Signal the game state that if has started
     */
    public void signalGameStart() {
        gameStarted = true;
        currentPlayer = whitePlayer;
    }

    public boolean isGameInPlay() { return gameStarted && !isCheckMate() && !isStaleMate(); }

    /**
     * Getter for the table
     * @return the current table of the game state
     */
    public Table table() {
        return table;
    }

    /**
     * Checker whether it is human turn or not
     * @return booean if current turn is on human
     */
    boolean isHumanTurn() {
        return currentPlayer == null;
    }

    /**
     * Checker whether it is AI turn or not
     * @return !isHumanTurn()
     */
    public boolean isAITurn() {
        return !isHumanTurn();
    }

    /**
     * Getter for next move of AI.
     * Throws InvalidStateException if the current turn is on human
     * @return Move chosen by the AI algorithm
     */
    public Optional<Move> getNextAIMove() {
        // dynamic difficulty
        int depth = AISearchDepth;
        int numOfPieces = table.getPieces().size();
        if (numOfPieces < 16)
            depth += 1;
        if (numOfPieces < 8)
            depth += 1;
        if (numOfPieces < 6)
            depth += 1;
        return currentPlayer.getMove(table, depth, playerTurn);
    }

    /**
     * Getter for current player turn
     * @return the color of the current player
     */
    public ChessColor getCurrentPlayerColor() {
        return playerTurn;
    }

    /**
     * Setter for AI search depth
     * @param value specifies the new search depth of the AI algorithm
     */
    public void setAISearchDepth(int value) {
        AISearchDepth = value;
    }

    /**
     * Getter for AI search depth
     * @return current search depth of the AI
     */
    public int getAISearchDepth() {
        return AISearchDepth;
    }

    /**
     * Getter to see if both players are AI.
     * @return true if both players are ai
     */
    public boolean areBothPlayersAI() {
        return whitePlayer != null && blackPlayer != null;
    }

    /**
     * Check whether the game is in checkmate
     * @return true if the game is in checkmate
     */
    public boolean isCheckMate() {
        return !table.isKingSafe(playerTurn) && table.getLegalMoves(playerTurn).isEmpty();
    }

    /**
     * Check whether the game is in stalemate
     * @return true if the game is in stalemate
     */
    public boolean isStaleMate() {
        return table.isKingSafe(playerTurn) && table.getLegalMoves(playerTurn).isEmpty();
    }

    private ChessColor playerTurn;
    private Table table;
    private ChessAI whitePlayer;
    private ChessAI blackPlayer;
    private ChessAI currentPlayer;
    private int AISearchDepth;
    private boolean gameStarted;
}
