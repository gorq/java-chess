package main.chess.engine;

import main.chess.utils.Move;
import main.gui.ChessGUI;

import java.util.Optional;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;

/**
 * Class controlling the moves of the chess.
 */
public class MoveController extends Thread {
    /**
     * Constructor.
     * @param gs is the game state
     * @param gc is the gui controller
     */
    public MoveController(GameState gs, ChessGUI gc, Lock moveLock, Condition awaitNextCond) {
        this.gameState = gs;
        this.guiController = gc;
        this.moveLock = moveLock;
        this.awaitNextAIMoveCondition = awaitNextCond;
    }

    /**
     *
     */
    @Override
    public void run() {
        super.run();
        while(gameState.isGameInPlay()) {
            try {
                moveLock.lock();
                while (gameState.isHumanTurn()) {
                    awaitNextAIMoveCondition.await();
                }
                // sleep for half a second if both players are AI
                if (gameState.areBothPlayersAI())
                    Thread.sleep(500);
                Optional<Move> toMake = gameState.getNextAIMove();
                if (!toMake.isPresent()) {
                    // checkmate or stalemate idk
                    break;
                }
                gameState.update(toMake.get());
                guiController.updateBoard();
                guiController.signalAIMove();
            } catch (InterruptedException e) {
                break;
            } finally {
                moveLock.unlock();
            }

        }
        guiController.signalAIMove();
    }

    private final GameState gameState;
    private final ChessGUI guiController;
    private final Lock moveLock;
    private final Condition awaitNextAIMoveCondition;
}
