package main.chess.engine.AI;

import main.chess.table.Table;
import main.chess.utils.Move;
import main.chess.utils.ChessColor;

import java.util.Optional;

/**
 * Interface to define a chess AI that will be able to execute moves on a given board.
 */
public interface ChessAI {
    /**
     * Decide what move is going to be taken next by the AI
     * @param t is the table upon which a move is to be made
     * @param timeout is the timeout for thread to stop looking for possible moves
     * @param playerTurn is the player that it's turn it is
     * @return the move gotten by this AI, or empty if no move found == checkmate
     */
    Optional<Move> getMove(Table t, int timeout, ChessColor playerTurn);
}
