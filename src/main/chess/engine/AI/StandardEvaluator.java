package main.chess.engine.AI;

import main.chess.table.Table;
import main.chess.pieces.Piece;
import main.chess.utils.ChessColor;

import java.io.Serializable;

/**
 * Standard evaluator of board based on ...
 */
public class StandardEvaluator implements BoardEvaluator, Serializable {
    /**
     * {@inheritDoc}
     */
    @Override
    public int evaluate(Table t) {
        int currentVal = 0;
        for (Piece p : t.getPieces()) {
            if (p.color() == ChessColor.WHITE)
                currentVal += p.value();
            else
                currentVal -= p.value();
        }
        return currentVal;
    }
}
