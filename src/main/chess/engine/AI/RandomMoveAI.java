package main.chess.engine.AI;

import main.chess.utils.Move;
import main.chess.pieces.Piece;
import main.chess.table.Table;
import main.chess.utils.ChessColor;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Optional;
import java.util.Random;

/**
 * A very simple chess AI that chooses a random move to make
 */
public class RandomMoveAI implements ChessAI, Serializable {
    /**
     * {@inheritDoc}
     */
    @Override
    public Optional<Move> getMove(Table table, int timeout, ChessColor playerTurn) {
        ArrayList<Piece> myPieces = new ArrayList<>();

        // get all the possible moves
        ArrayList<Move> possibleMoves = new ArrayList<>();
        for(Piece p : table.getPieces()) {
            // if piece is different color, skip it
            if (p.color() != playerTurn)
                continue;
            for(int i = 0; i < 8; ++i) {
                for(int j = 0; j < 8; ++j) {
                    if (p.canMoveTo(table.getTile(j, i), table))
                        possibleMoves.add(new Move(p.getTile().column(), p.getTile().row(), j, i));
                }
            }
        }

        if (possibleMoves.isEmpty())
            return Optional.empty();
        return Optional.of(possibleMoves.get(Math.abs(new Random(100).nextInt())%possibleMoves.size()));
    }
}
