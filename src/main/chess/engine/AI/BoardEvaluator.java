package main.chess.engine.AI;

import main.chess.table.Table;

/**
 * Interface for evaluating a game board.
 */
public interface BoardEvaluator {
    /**
     * Evaluate a chess table.
     * @param t is the table to evaluate
     * @return the integer value of the given table
     */
    int evaluate(Table t);
}
