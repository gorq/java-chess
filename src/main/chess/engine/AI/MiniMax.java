package main.chess.engine.AI;

import main.chess.utils.Move;
import main.chess.table.Table;
import main.chess.utils.ChessColor;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Optional;
import java.util.Random;

/**
 * A MiniMax algorithm using Alpha-beta pruning to choose an AI move
 */
public class MiniMax implements ChessAI, Serializable {
    /**
     * Class to save values of corresponding moves
     */
    private static class ValuedMove implements Comparable<ValuedMove>{
        public Move move;
        public int value;

        /**
         * Simple constructor
         * @param m is the move to be saved
         * @param v is the value to be saved
         */
        ValuedMove(Move m, int v) {
            move = m;
            value = v;
        }

        /**
         * Compare this ValuedMove with other, based on their value attribute
         * @param other os the other move we are comparing to
         * @return Integer compare of values
         */
        @Override
        public int compareTo(ValuedMove other) {
            return Integer.compare(value, other.value);
        }
    }

    /**
     * MiniMax constructor
     */
    public MiniMax() {
        boardEvaluator = new StandardEvaluator();
        randomizer = new Random();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Optional<Move> getMove(Table t, int depth, ChessColor playerTurn) {
        // start depth control

        ArrayList<Move> bestMoves = new ArrayList<>();
        if (playerTurn == ChessColor.WHITE) {
            int currentBestScore = Integer.MIN_VALUE;
            for (Move m : t.getLegalMoves(playerTurn)) {
                Table tmp = new Table(t);
                tmp.makeMove(m, playerTurn);
                if (!tmp.isKingSafe(playerTurn))
                    continue;
                int moveValue = minimax(tmp, depth, Integer.MIN_VALUE, Integer.MAX_VALUE, ChessColor.BLACK);
                if (moveValue > currentBestScore) {
                    currentBestScore = moveValue;
                    bestMoves = new ArrayList<>();
                    bestMoves.add(m);
                }
                else if (moveValue == currentBestScore) {
                    bestMoves.add(m);
                }
            }
        }
        else {
            int currentBestScore = Integer.MAX_VALUE;
            for(Move m : t.getLegalMoves(playerTurn)) {
                Table tmp = new Table(t);
                tmp.makeMove(m, playerTurn);
                if (!tmp.isKingSafe(playerTurn))
                    continue;
                int moveValue = minimax(tmp, depth, Integer.MIN_VALUE, Integer.MAX_VALUE, ChessColor.WHITE);
                if (moveValue < currentBestScore) {
                    currentBestScore = moveValue;
                    bestMoves = new ArrayList<>();
                    bestMoves.add(m);
                }
                else if (moveValue == currentBestScore) {
                    bestMoves.add(m);
                }
            }
        }

        if (bestMoves.isEmpty())
            return Optional.empty();
        return Optional.of(bestMoves.get((Math.abs(randomizer.nextInt())%bestMoves.size())));
    }

    /**
     * Minimax algorithm using alpha-beta pruning
     * @param t is the table on which we are moving
     * @param alpha is the current alpha parameter
     * @param beta is the current beta parameter
     * @param currentPlayer is the current player moving
     * @return the board value after moves chosen by the algorithm
     */
    private int minimax(Table t, int depth, int alpha, int beta, ChessColor currentPlayer) {
        // check if we have to stop looking for moves
        if (depth == 0)
            return boardEvaluator.evaluate(t);

        ArrayList<ValuedMove> sortedMoves = sortMoves(t, currentPlayer);
        if (currentPlayer == ChessColor.WHITE) {
            int currentBest = Integer.MIN_VALUE;
            for(ValuedMove m : sortedMoves) {
                Table tmp = new Table(t);
                tmp.makeMove(m.move, currentPlayer);
                if (!tmp.isKingSafe(currentPlayer))
                    continue;
                int valueOfMove = minimax(tmp, depth-1, alpha, beta, ChessColor.BLACK);
                currentBest = Math.max(currentBest, valueOfMove);
                alpha = Math.max(alpha, valueOfMove);
                if (beta <= alpha)
                    break;
            }
            return currentBest;
        }

        else {
            int currentBest = Integer.MAX_VALUE;
            for(ValuedMove m : sortedMoves) {
                Table tmp = new Table(t);
                tmp.makeMove(m.move, currentPlayer);
                if (!tmp.isKingSafe(currentPlayer))
                    continue;
                int valueOfMove = minimax(tmp, depth-1, alpha, beta, ChessColor.WHITE);
                currentBest = Math.min(currentBest, valueOfMove);
                beta = Math.min(beta, valueOfMove);
                if (beta <= alpha)
                    break;
            }
            return currentBest;
        }
    }

    /**
     * Sort moves based on the evaluator of this class.
     * Order is ascending for black player, and descending for white player
     * @param t is the table on which we will evaluate the moves
     * @param c is the player color of which we are checking moves
     * @return sorted moves
     */
    private ArrayList<ValuedMove> sortMoves(Table t, ChessColor c) {
        ArrayList<ValuedMove> result = new ArrayList<>();
        if (c == ChessColor.WHITE) {
            for (Move m : t.getLegalMoves(c)) {
                Table tmp = new Table(t);
                tmp.makeMove(m, c);
                int valueOfMove = boardEvaluator.evaluate(tmp);
                result.add(new ValuedMove(m, valueOfMove));
            }
            Collections.sort(result, Collections.reverseOrder());
        }
        else {
            for (Move m : t.getLegalMoves(c)) {
                Table tmp = new Table(t);
                tmp.makeMove(m, c);
                int valueOfMove = boardEvaluator.evaluate(tmp);
                result.add(new ValuedMove(m, valueOfMove));
            }
            Collections.sort(result);
        }

        return result;
    }

    private final BoardEvaluator boardEvaluator;
    private final Random randomizer;
}
