package main.gui;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import main.chess.Model;
import main.chess.engine.GameState;
import main.chess.utils.GameConfig;

import java.io.IOException;

public class GUIController extends Application {
    public static void load() {
        launch();
    }

    @Override
    public void start(Stage stage) throws Exception {
        workingStage = stage;
        // load all scenes
        welcomeScene = new Scene(loadParent("mainFrame.fxml"));
        newGameScene = new Scene(loadParent("newGameFrame.fxml"));
        loadGameScene = new Scene(loadParent("loadGameFrame.fxml"));
        activeGameScene = new Scene(loadParent("activeGameFrame.fxml"));
        stage.setTitle(Constants.APP_TITLE_NAME);
        stage.setScene(welcomeScene);
        stage.setResizable(false);
        stage.show();
        // setup so that chess engine stops when app is closed
        stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent windowEvent) {
                Model.CHESS.interruptMoveController();
            }
        });
        ACTIVE = this;
    }

    public void loadNewGameScene() {
        workingStage.setScene(newGameScene);
    }

    public void loadWelcomeScene() {
        workingStage.setScene(welcomeScene);
    }

    public void loadLoadGameScene() {
        LoadGameFrame.ACTIVE.reloadSaves();
        workingStage.setScene(loadGameScene);
    }

    public void announce(String s, Alert.AlertType type) {
        Alert e = new Alert(type);
        e.setHeaderText(s);
        e.show();
    }

    public void startGame(GameConfig cfg) {
        ActiveGameFrame.setup(cfg);
        workingStage.setScene(activeGameScene);
    }

    public void loadGame(GameState gs) {
        ActiveGameFrame.setup(gs);
        workingStage.setScene(activeGameScene);
    }

    private Parent loadParent(String fxml) throws IOException {
        return FXMLLoader.load(getClass().getResource(fxml));
    }


    private Stage workingStage;
    private Scene welcomeScene;
    private Scene newGameScene;
    private Scene loadGameScene;
    private Scene activeGameScene;

    public static GUIController ACTIVE;
}
