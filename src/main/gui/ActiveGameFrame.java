package main.gui;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextInputDialog;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import main.chess.Model;
import main.chess.engine.Chess;
import main.chess.engine.GameState;
import main.chess.utils.ChessColor;
import main.chess.utils.Move;
import main.chess.pieces.Piece;
import main.chess.table.Tile;
import main.chess.utils.GameConfig;

import java.io.*;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public class ActiveGameFrame implements Initializable, ChessGUI {
    @Override
    public void start() {

    }

    @Override
    public void updateBoard() {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                redrawBoard();
            }
        });
    }

    @Override
    public void signalAIMove() {
        checkGameEnd();
    }

    private void checkGameEnd() {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                if (Model.CHESS.gameState().hasEnded() && !ACTIVE.wasResultAnnounced) {
                    ACTIVE.wasResultAnnounced = true;
                    announceResult();
                }
            }
        });
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        ACTIVE = this;
        ACTIVE.moveLock = new ReentrantLock();
        ACTIVE.awaitNextAIMoveCondition = ACTIVE.moveLock.newCondition();
        ACTIVE.boardChangeLock = new ReentrantLock();
        ACTIVE.isBoardFlipped = false;
        populateButtons();
        try {
            populateImages();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        Model.CHESS = new Chess(ACTIVE, ACTIVE.gridButtons, ACTIVE.moveLock, ACTIVE.awaitNextAIMoveCondition);
    }

    public static void setup(GameConfig cfg) {
        Model.CHESS.startNewGame(cfg);
        ACTIVE.redrawBoard();
    }

    public static void setup(GameState gs) {
        Model.CHESS.loadGame(gs);
        ACTIVE.redrawBoard();
        String playerColor = Model.CHESS.gameState().getCurrentPlayerColor().toString();
        playerColor = playerColor.substring(0, 1).toUpperCase() + playerColor.substring(1).toLowerCase();
        GUIController.ACTIVE.announce(playerColor + " is on turn.", Alert.AlertType.INFORMATION);
    }

    /**
     * Check current game state and announce appropriate ressults
     */
    private void announceResult() {
        redrawBoard();
        if (Model.CHESS.gameState().isCheckMate())
            GUIController.ACTIVE.announce(ChessColor.swapColor(Model.CHESS.gameState().getCurrentPlayerColor()) + " won!", Alert.AlertType.INFORMATION);
        else
            GUIController.ACTIVE.announce("Draw.", Alert.AlertType.INFORMATION);
    }



    private void populateButtons() {
        int width = grid.getColumnCount();
        int height = grid.getRowCount();
        gridButtons = new Button[height][width];
        for(int row = 0; row < height; ++row) {
            for(int col = 0; col < width; ++col) {
                Button b = new Button();
                b.setPrefSize(Double.MAX_VALUE, Double.MAX_VALUE);
                int finalRow = row;
                int finalCol = col;
                b.setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent actionEvent) {
                        if (isBoardFlipped)
                            processClick(7- finalRow, 7- finalCol);
                        else
                            processClick(finalRow, finalCol);
                    }
                });
                if ((row+col)%2 == 0)
                    b.setStyle("-fx-background-color: darkgrey");
                else
                    b.setStyle("-fx-background-color: white");
                grid.add(b, col, row);
                gridButtons[row][col] = b;
            }
        }
    }

    /**
     * Process a mouse-click done on the panel
     * @param row is the row
     * @param column is the column
     */
    private void processClick(int row, int column) {
        if (!Model.CHESS.gameState().hasStarted() || Model.CHESS.gameState().hasEnded() || Model.CHESS.gameState().isAITurn())
            return;
        try {
            moveLock.lock();
            // cancel if game has not started yet        t
            Tile clickedTile = Model.CHESS.gameState().table().getTile(column, row);
            // choosing which piece to move
            if (humanTileSelected == null
                    && clickedTile.isTaken()
                    && clickedTile.piece().get().color().toString().equals(Model.CHESS.gameState().getCurrentPlayerColor().toString())) {
                humanTileSelected = clickedTile;
            }
            // else choosing where to move already picked piece
            else if (humanTileSelected != null) {
                Move moveToMake = new Move(
                        humanTileSelected.column(),
                        humanTileSelected.row(),
                        clickedTile.column(),
                        clickedTile.row()
                );
                if (humanTileSelected.piece().get().canMoveTo(clickedTile, Model.CHESS.gameState().table()) && Model.CHESS.willKingBeSafe(moveToMake)) {
                    Model.CHESS.makeMove(moveToMake);
                    redrawBoard();
                    checkGameEnd();
                    humanTileSelected = null;

                    // run AI turn if it is his turn
                    if (Model.CHESS.gameState().isAITurn()) {
                        // just notify the ai that a move has been made by a human
                        awaitNextAIMoveCondition.signal();
                    }
                }
                else if (clickedTile.piece().isPresent()
                        && clickedTile.piece().get().color().equals(Model.CHESS.gameState().getCurrentPlayerColor())) {
                    humanTileSelected = clickedTile;
                }
                else
                    humanTileSelected = null;
            }
        }
        finally {
            moveLock.unlock();
        }
    }


    /**
     * Populate images map with corresponding files
     */
    private void populateImages() throws FileNotFoundException {
        images = new HashMap<>();
        String[] colors = {"black", "white"};
        String[] pieces = {"Pawn", "Knight", "Bishop", "Rook", "Queen", "King"};
        String piecesPath = "/main/resources/images/pieces";
        for (String color : colors) {
            for (String piece : pieces) {
                String piecePath = piecesPath+"/"+color+"/"+piece;
                URL pieceURL = getClass().getResource(piecePath);
                Image img = new Image(new FileInputStream(pieceURL.getPath()));
                images.put(color+piece, img);
            }
        }
    }

    /**
     * Redraw the entire board.
     */
    private void redrawBoard() {
        try {
            boardChangeLock.lock();
            for (int i = 0; i < 8; ++i) {
                for (int j = 0; j < 8; ++j) {
                    if (!Model.CHESS.gameState().table().getTile(j, i).isTaken()) {
                        if (isBoardFlipped) {
                            gridButtons[7 - i][7 - j].setGraphic(null);
                        }
                        else
                            gridButtons[i][j].setGraphic(null);
                    }
                    else {
                        Piece p = Model.CHESS.gameState().table().getTile(j, i).piece().get();
                        String color = p.color().toString().toLowerCase();
                        ImageView imageToShow = new ImageView(images.get(color + p.toString()));
                        imageToShow.setFitHeight(IMAGE_SIZE);
                        imageToShow.setFitWidth(IMAGE_SIZE);
                        if (isBoardFlipped)
                            gridButtons[7-i][7-j].setGraphic(imageToShow);
                        else
                            gridButtons[i][j].setGraphic(imageToShow);
                    }
                }
            }
        }
        finally {
            boardChangeLock.unlock();
        }
    }

    public void onFlipBoardClick() {
        isBoardFlipped = !isBoardFlipped;
        redrawBoard();
    }

    public void onSaveGameClick() {
        if (Model.CHESS.gameState().isAITurn()) {
            GUIController.ACTIVE.announce("A move is being made, you have to wait.", Alert.AlertType.ERROR);
            return;
        }
        TextInputDialog inputDialog = new TextInputDialog();
        inputDialog.setTitle("Save game");
        inputDialog.setHeaderText("Enter desired save file name");
        inputDialog.setContentText("Save file name:");
        Optional<String> result = inputDialog.showAndWait();
        result.ifPresent(this::saveCurrentGame);
    }

    public void onExitGameClick() {
        Alert a = new Alert(Alert.AlertType.CONFIRMATION);
        a.setTitle("Exit game");
        a.setHeaderText("Attempting to exit game");
        a.setContentText("Do you really want to exit game?");
        Optional<ButtonType> result = a.showAndWait();
        if (result.get() == ButtonType.OK) {
            Model.CHESS.interruptMoveController();
            GUIController.ACTIVE.loadWelcomeScene();
        }
    }

    /**
     * Save the current game state to a file
     * @param saveFileName specifies the filename of the save file
     */
    private void saveCurrentGame(String saveFileName) {
        if (saveFileName == null)
            return;
        if (Model.EXISTING_SAVES_NAMES.contains(saveFileName)) {
            GUIController.ACTIVE.announce("Save file with such name already exists!", Alert.AlertType.ERROR);
            return;
        }
        try (FileOutputStream out = new FileOutputStream("saves/"+saveFileName);
             ObjectOutputStream oOut = new ObjectOutputStream(out)) {
            oOut.writeObject(Model.CHESS.gameState());
            oOut.flush();
            GUIController.ACTIVE.announce("Game saved successfully.", Alert.AlertType.INFORMATION);
            Model.reloadSaves();
        } catch (IOException e) {
            GUIController.ACTIVE.announce("There was a problem saving your game.", Alert.AlertType.ERROR);
            e.printStackTrace();
        }
    }



    // attributes
    private final static int IMAGE_SIZE = 50;

    @FXML
    private GridPane grid;

    private static ActiveGameFrame ACTIVE;
    private Button[][] gridButtons;
    private boolean isBoardFlipped;
    private ReentrantLock boardChangeLock, moveLock;
    private Condition awaitNextAIMoveCondition;
    private Map<String, Image> images;
    private Tile humanTileSelected;

    private volatile boolean wasResultAnnounced;
}
