package main.gui;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;

import java.net.URL;
import java.util.ResourceBundle;

public class MainFrame implements Initializable {
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
    }

    /**
     * Handle load game press.
     */
    public void onLoadGamePress() {
        GUIController.ACTIVE.loadLoadGameScene();
    }

    /**
     * Handle new game press.
     */
    public void onNewGamePress() {
        GUIController.ACTIVE.loadNewGameScene();
    }
}
