package main.gui;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.input.MouseEvent;
import main.chess.Model;
import main.chess.engine.GameState;

import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.net.URL;
import java.util.ResourceBundle;

public class LoadGameFrame implements Initializable {
    public static LoadGameFrame ACTIVE;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        // populate scroll pane
        if (Model.EXISTING_SAVES_NAMES.size() != 0) {
            ObservableList<String> items = FXCollections.observableArrayList(Model.EXISTING_SAVES_NAMES.toArray(new String[1]));
            savesNamesBox.setItems(items);
        }
        // disable needed buttons initially
        resetButtons();
        // allow to click it once item is selected
        savesNamesBox.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                startGameButton.setDisable(false);
                deleteSaveButton.setDisable(false);
            }
        });
        ACTIVE = this;
    }

    private void resetButtons() {
        startGameButton.setDisable(true);
        deleteSaveButton.setDisable(true);
    }

    public void reloadSaves() {
        resetButtons();
        Model.reloadSaves();
        if (Model.EXISTING_SAVES_NAMES.size() != 0) {
            ObservableList<String> items = FXCollections.observableArrayList(Model.EXISTING_SAVES_NAMES.toArray(new String[1]));
            savesNamesBox.setItems(items);
        }
        else {
            savesNamesBox.setItems(null);
        }
    }

    public void onGoBackClicked() {
        GUIController.ACTIVE.loadWelcomeScene();
    }

    public void onStartGameClicked() {
        GameState toStart = loadGame(savesNamesBox.getSelectionModel().getSelectedItem());
        GUIController.ACTIVE.loadGame(toStart);
    }

    public void onSaveDelete() {
        Model.deleteSave(savesNamesBox.getSelectionModel().getSelectedItem());
        savesNamesBox.getItems().remove(savesNamesBox.getSelectionModel().getSelectedItem());
        resetButtons();
        savesNamesBox.getSelectionModel().clearSelection();
    }


    /**
     * Load a save file of given name
     * @param saveFileName specifies the save file name
     * @return the game state saved in the file
     */
    private GameState loadGame(String saveFileName) {
        try (FileInputStream in = new FileInputStream("saves/"+saveFileName);
             ObjectInputStream oIn = new ObjectInputStream(in)) {
            return (GameState) oIn.readObject();
        } catch (Exception e) {
            GUIController.ACTIVE.announce("Load files folder is corrupt.", Alert.AlertType.ERROR);
            return null;
        }
    }

    @FXML
    private Button startGameButton;

    @FXML
    private Button deleteSaveButton;

    @FXML
    private ListView<String> savesNamesBox;
}
