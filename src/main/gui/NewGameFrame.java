package main.gui;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import main.chess.utils.PlayerType;
import main.chess.utils.GameConfig;

import java.net.URL;
import java.util.ResourceBundle;

public class NewGameFrame implements Initializable {
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        ToggleGroup radioButtonsGroup = new ToggleGroup();
        easyDifficulty.setToggleGroup(radioButtonsGroup);
        mediumDifficulty.setToggleGroup(radioButtonsGroup);
        hardDifficulty.setToggleGroup(radioButtonsGroup);
        easyDifficulty.setSelected(true);

        // set selected players
        whiteHumanButton.setSelected(true);
        blackComputerButton.setSelected(true);
    }

    public void onWhiteAIClicked() {
        handleSelectionClick(whiteComputerButton, whiteHumanButton);
    }

    public void onWhiteHumanClicked() {
        handleSelectionClick(whiteHumanButton, whiteComputerButton);
    }

    public void onBlackAIClicked() {
        handleSelectionClick(blackComputerButton, blackHumanButton);
    }

    public void onBlackHumanClicked() {
        handleSelectionClick(blackHumanButton, blackComputerButton);
    }

    public void onGoBackClicked() {
        GUIController.ACTIVE.loadWelcomeScene();
    }

    public void onStartGameClicked() {
        PlayerType whitePlayer = whiteHumanButton.isSelected() ? PlayerType.PERSON : PlayerType.AI;
        PlayerType blackPlayer = blackHumanButton.isSelected() ? PlayerType.PERSON : PlayerType.AI;
        GUIController.ACTIVE.startGame(
                new GameConfig(getSelectedDifficulty(), whitePlayer, blackPlayer)
        );
    }

    private int getSelectedDifficulty() {
        if (easyDifficulty.isSelected())
            return 2;
        else if (mediumDifficulty.isSelected())
            return 3;
        else
            return 4;
    }

    private void handleSelectionClick(ToggleButton a, ToggleButton b) {
        if (!a.isSelected())
            a.setSelected(true);
        else
            b.setSelected(false);
    }

    @FXML
    private ToggleButton whiteComputerButton;
    @FXML
    private ToggleButton whiteHumanButton;
    @FXML
    private ToggleButton blackComputerButton;
    @FXML
    private ToggleButton blackHumanButton;
    @FXML
    private RadioButton easyDifficulty;
    @FXML
    private RadioButton mediumDifficulty;
    @FXML
    private RadioButton hardDifficulty;
}
