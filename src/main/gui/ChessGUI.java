package main.gui;

/**
 * Interface describing capabilities of chess gui.
 */
public interface ChessGUI {
    /**
     * Method to start up the gui.
     */
    void start();

    /**
     * Update board method.
     */
    void updateBoard();

    /**
     * Signal ai move made by the engine.
     */
    void signalAIMove();
}
